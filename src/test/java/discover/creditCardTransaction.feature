Feature: Transaction pricing calculation

   Scenario Outline: Checking flow with correct transaction data
    Given Customer transaction data is available at "<inputDataFileLocation>"
    When "<inputDataFileLocation>" data is processed by program
    And Result file is generated at "<actualResultsFileLocation>"
    Then Data from "<actualResultsFileLocation>" matches data from "<expectedResultsFileLocation>"

    Examples:
      | inputDataFileLocation             | expectedResultsFileLocation           | actualResultsFileLocation             |
      | src/test/resources/inputData.csv  | src/test/resources/expectedResults.csv| src/test/resources/actualResults.csv  |


   Scenario Outline: Checking flow with transaction data missing values
    Given Customer transaction data is available at "<inputDataFileLocation>"
    When "<inputDataFileLocation>" data is being loaded by program
    Then Program returns error msg

    Examples:
      | inputDataFileLocation
      | src/test/resources/inputData.csv

   Scenario Outline: Checking flow with transaction data containing too much fields
    Given Customer transaction data is available at "<inputDataFileLocation>"
    When "<inputDataFileLocation>" data is being loaded by program
    Then Program returns error msg

    Examples:
       | inputDataFileLocation
       | src/test/resources/inputData.csv

   Scenario Outline: Checking flow with transaction data containing incorrect issuer prefix
    Given Customer transaction data is available at "<inputMissingDataFileLocation>"
    When "<inputDataFileLocation>" data is processed by program
    And Result file is generated at "<actualResultsFileLocation>"
    Then Data from "<actualResultsFileLocation>" matches data from "<expectedResultsFileLocation>"

    Examples:
      | inputMissingDataFileLocation             | expectedResultsFileLocation           | actualResultsFileLocation             |
      | src/test/resources/inputMissingData.csv  | src/test/resources/expectedResults.csv| src/test/resources/actualResults.csv  |