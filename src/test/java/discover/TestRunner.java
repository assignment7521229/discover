package discover;

import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;
import io.cucumber.junit.Cucumber;
@RunWith(Cucumber.class)
@CucumberOptions(
        features ="src/test/java/discover",
        glue="discover", stepNotifications = true
)
public class TestRunner {

}
