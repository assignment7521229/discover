package discover;
import com.opencsv.CSVReader;
import com.opencsv.exceptions.CsvValidationException;
import io.cucumber.java.en.Then;
import org.example.mockProgram;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import org.junit.Assert;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class stepDefinitions {
    @Given("Customer transaction data is available at {string}")
    public void  customerTransactionDataIsAvailable(String inputLocation){
        File file = new File(inputLocation);

        assertEquals(true, file.exists());
    }

    @When("{string} data is processed by program")
    public boolean dataProcessed(String inputFileLocation) {
        try {
             mockProgram.mockProgram(inputFileLocation);
        } catch (Exception e) {
        return false;
        }
       return true;
    }


    @When("Result file is generated at {string}")
    public void actualResultsAreAvailable(String inputLocation){
        File file = new File(inputLocation);
        assertEquals(true, file.exists());
    }

    @Then("Data from {string} matches data from {string}")
    public void actualResultsMatchExpectedResults (String actualResultsLocation, String expectedResultsLocation) {
        List<String[]> expectedData = readCSVFile(expectedResultsLocation);
        List<String[]> actualData = readCSVFile(actualResultsLocation);

        try{
        for (int i = 0; i < expectedData.size(); i++) {
            String[] expectedRow = expectedData.get(i);
            String[] actualRow = actualData.get(i);

            assertEquals(expectedRow.length, actualRow.length);

            for (int j = 0; j < expectedRow.length; j++) {
                assertEquals(expectedRow[j], actualRow[j]);
            }
        }} catch (IndexOutOfBoundsException e){
            e.printStackTrace();
        }
    }

    private List<String[]> readCSVFile(String filePath) {
        List<String[]> csvData = new ArrayList<>();

        try (CSVReader reader = new CSVReader(new FileReader(filePath))) {
            String[] nextLine;
            while ((nextLine = reader.readNext()) != null) {
                csvData.add(nextLine);
            }
        }
         catch (IOException | CsvValidationException | IndexOutOfBoundsException e) {
            e.printStackTrace();
        }
        return csvData;
    }

    @When ("{string} data is being loaded by program")
    public boolean dataWithErrorProcessed(String inputFileLocation) {

    //to do
        try {
            mockProgram.mockProgram(inputFileLocation);
        } catch (Exception e) {
            return false;
        }
        return true;
    }
    @Then ("Program returns error msg")
    public void errorMsgReturned(){
        //to do
        String actualErrorMsg = "error";
        Assert.assertEquals("expected error msg", actualErrorMsg);
    }

}


